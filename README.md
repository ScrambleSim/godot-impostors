![screen](https://gitlab.com/ScrambleSim/godot-impostors/-/wikis/uploads/26d761be299aaca6870087e25f60326f/screen.png)

# About
Godot implementation of octahedral impostor rendering.

# Getting started

1. Download Godot 3.1.2 Standard version: https://godotengine.org/download
2. Clone this project
3. Open the `.project` file in Godot
4. Check out `ImpostorShader.shader`

# Credits

This project is a Godot port of the amazing https://github.com/xraxra/IMP/.
