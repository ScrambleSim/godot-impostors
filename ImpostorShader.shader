shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_metallic : hint_white;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

varying vec3 camDir;
varying vec2 grid;

// CS -> Camera Space
// WS -> World Space
// MS -> Model/Object/Local Space

// Create look at matrix which rotates billboards toward a target.
// No rotation around roll axis, only pitch and yaw.
// Lookat matrix is created by taking 3 orthogonal vectors, of which
// one (forward) is pointing towards the target. These are put into a mat4.
// Source: http://www.songho.ca/opengl/gl_lookattoaxes.html
mat4 calcLookAtMatrixBillboard(vec3 originPos_WS, vec3 targetPos_WS) {
	vec3 forward = normalize(targetPos_WS - originPos_WS);
	vec3 left = normalize(cross(vec3(0.0, 1.0, 0.0), forward));
	vec3 up = normalize(cross(forward, left));

	return mat4(
		vec4(left.x, up.x, forward.x, 0),
		vec4(left.y, up.y, forward.y, 0),
		vec4(left.z, up.z, forward.z, 0),
		vec4(0, 0, 0, 1)
	);
}


vec2 VecToSphereOct( vec3 pivotToCamera )
{
    // Convert because Godot has right handed coordinate system but this Unity logic is left handed
    pivotToCamera = vec3(pivotToCamera.x, pivotToCamera.y, -pivotToCamera.z);
    
    pivotToCamera.xy /= dot( vec3(1, 1, 1),  abs(pivotToCamera) );
    if ( pivotToCamera.z <= 0.0 )
    {
        vec2 flip = (pivotToCamera.x >= 0.0 && pivotToCamera.y >= 0.0) ? vec2(1,1) : vec2(-1,-1);
        pivotToCamera.xy = (1.0 - abs(pivotToCamera.yx)) * flip;
    }

    return pivotToCamera.xy;
}


void vertex() {
	// Convert camera and object origin from local to world coordinates
	vec3 cameraPos_WS = (CAMERA_MATRIX * vec4(vec3(0, 0, 0), 1.0)).xyz;
	vec3 objectPos_WS = (WORLD_MATRIX * vec4(vec3(0, 0, 0), 1.0)).xyz;

	// Rotate vertex towards the camera using the model origin as pivot.
	VERTEX = (vec4(VERTEX, 1) * calcLookAtMatrixBillboard(objectPos_WS, cameraPos_WS)).xyz;

	// pass camera direction to fragment shader
	camDir = normalize(cameraPos_WS - objectPos_WS);

	// pass position in grid map to fragment shader
	vec2 tmp = VecToSphereOct(camDir);
	tmp = (tmp + vec2(1, 1)) * 0.5;		// bias and scale to 0-1 range
	tmp = clamp(tmp, vec2(0, 0), vec2(1, 1));
	grid = tmp * 15.0;
    grid = floor(grid);
}


void fragment() {
	vec2 base_uv = vec2(1.0/16.0, 1.0/16.0);
	base_uv = (UV / 16.0) + (grid / 16.0);
	vec4 albedo_tex = texture(texture_albedo, base_uv);
	ALBEDO = albedo_tex.xyz;
	ALPHA = albedo_tex.a;
}
